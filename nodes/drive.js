/**
 * Copyright 2014 IBM Corp.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

module.exports = function(RED) {
    "use strict";
    var fs = require('fs');
    var path = require('path');
    var mt = require('mime-types');
    
    function checkCredentials(node) {
        if (!node.google || !node.google.credentials.accessToken) {
            noCredentials(node);
            return false;    
        }
        return true;
    }

    function noCredentials(node) {
        node.warn(RED._("drive.warn.no-credentials"));
        node.status({fill:"red",shape:"dot",text:"drive.status.error.missing-credentials"});
    }

    function initateUpload(node, metadata, cb) {
        if (!node.google || !node.google.credentials.accessToken) {
            node.warn(RED._("drive.warn.no-credentials"));
            return;
        }
        node.google.request({ 
            url: 'https://www.googleapis.com/upload/drive/v3/files?uploadType=resumable', 
            method: 'POST',
            body: metadata,
            headers: { 
                "Content-Type": "application/json"
            }
        }, function(err, response, data) {
            if (err || !response.headers || !response.headers["location"]) cb(err, null);
            else cb(null,  response.headers["location"]);
        });
    }

    function finishUpload(node, req, data, cb) {
        if (typeof req !== "object" || !req.url) req = { url: req };
        setDefault(req, 'method', 'PUT');
        setDefault(req, 'headers', { "Content-Length": data.length });
        setDefault(req, 'body', data);
        
        if (!checkCredentials(node)) return;

        node.google.request(req, function(err, response) {
            if (err) cb(err, null);
            else cb(null, response);
        });
    }

    function check(node, msg, variable) {
        if (!msg || !msg[variable]) {
            node.status({fill:"red",shape:"dot",text:"drive.error.parameter-" + variable});
            return false;
        }   
        return true;
    }

    function setDefault(msg, variable, defaultValue) {
        if (!msg[variable])
            msg[variable] = defaultValue;
    }
        
    function GoogleDriveUploadNode(n) {
        RED.nodes.createNode(this,n);
        var node = this;
        node.google = RED.nodes.getNode(n.google);
        node.on('input', function(msg) {
            if (!check(node, msg, "filename") && !check(node, n, "filename")) return;
            if (!check(node, msg, "filepath") && !check(node, n, "filepath")) return;
            setDefault(msg, 'mimeType', 'text/plain');

            var filename = msg.filename || n.filename;
            var extension = path.extname(filename);
            var mimetype = msg.mimetype || mt.lookup(extension);
            var metadata = { 
                name: filename, 
                mimeType: mimetype
            };
            
            this.status({fill:"blue",shape:"ring",text:"drive.status.initializing-upload"});
            initateUpload(node, metadata, (err, url) => {
                if (err) {
                    node.error(RED._("drive.error.error-in-upload-initializing"));
                    node.status({fill:"red",shape:"dot",text:"drive.error.error-in-upload"});
                    node.send(err);
                } 

                node.status({fill:"blue",shape:"ring",text:"drive.status.finishing-upload"});

                var req = { 
                    url: url,
                    encoding: null,
                    json: false
                };           
                
                var filepath = msg.filepath || n.filepath;
                var fullpath = path.join(filepath, filename);
                var body = fs.createReadStream(fullpath);
                finishUpload(node, req, body, (err, result) => {
                    if (err) {
                        node.error(RED._("drive.error.error-in-upload"));
                        node.status({fill:"red",shape:"dot",text:"drive.error.error-in-upload-finishing"});
                        node.send(err);
                    } else {
                        node.status({fill:"green",shape:"dot",text:"drive.status.uploadet"});
                        msg.payload = url;
                        node.send(msg);
                    }
                })
            });
        });
    }
    RED.nodes.registerType("google drive upload out", GoogleDriveUploadNode);
    
    function listFiles(node, msg, cb) {
        node.status({fill:"blue",shape:"ring",text:"drive.status.querying"});
        var q = "";
        if (node.query || msg.payload.query)
            q = "&q=" + (node.query || msg.payload.query);

        if (!checkCredentials(node)) return;

        var url = 'https://www.googleapis.com/drive/v3/files?pageSize=100' + q;
        node.warn(url);
        node.google.request({ 
            url: url, 
            method: 'GET',
            headers: {"Content-Type": "application/json" }
        }, function(err, response) {
            if (err) cb(err, null);
            else {
                if (response.statusCode === 200 && response.body && response.body.files) {
                    cb(null, response.body.files);
                } else {
                    cb({ "error" : "Files could not be found on the return object.", "response": response}, null)
                }
            }
        });
    }

    function GoogleDriveListNode(n) {
        RED.nodes.createNode(this,n);
        var node = this;
        node.google = RED.nodes.getNode(n.google);
        node.on('input', function(msg) {
            listFiles(node, msg, (err, files) => {
                if (err) {
                    node.error(RED._("drive.error.error.error-in-query"));
                    node.status({fill:"red",shape:"dot",text:"drive.error.error-in-query"});
                    msg.error = err;
                    node.send(msg);
                } else {
                    node.status({fill:"green",shape:"dot",text:"drive.status.finished"});
                    msg.files = files;
                    node.send(msg);
                }
            });
        });
    }
    RED.nodes.registerType("google drive list in", GoogleDriveListNode);

    function deleteFile(node, id, cb) {
        if (!checkCredentials(node)) return;

        node.google.request({ 
            url: 'https://www.googleapis.com/drive/v3/files/' + id, 
            method: 'DELETE',
            headers: {"Content-Type": "application/json" }
        }, function(err, response) {
            if (err) cb(err, null);
            else {
                if (response.statusCode === 204) {
                    cb(null, response);
                } else {
                    cb({ "error" : "File (" + id + ") could not be deleted.", "response": response}, null)
                }
            }
        });
    }

    function GoogleDriveDeleteNode(n) {
        RED.nodes.createNode(this,n);
        var node = this;
        node.google = RED.nodes.getNode(n.google);
        node.on('input', function(msg) {
            node.status({fill:"blue",shape:"ring",text:"drive.status.deleting"});
            deleteFile(node, msg.fileid, (err, result) => {
                if (err) {
                    node.error(RED._("drive.error.error.error-in-deleting"));
                    node.status({fill:"red",shape:"dot",text:"drive.error.error-in-deleting"});
                    msg.error = err;
                    node.send(msg);
                } else {
                    node.status({fill:"green",shape:"dot",text:"drive.status.finished"});
                    node.send(msg);
                }
            });
        });

    }
    RED.nodes.registerType("google drive delete in", GoogleDriveDeleteNode);
};