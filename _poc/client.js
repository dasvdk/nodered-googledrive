var auth = require("./auth")
const fs = require('fs');
const {google} = require('googleapis');

function generic(funcName, props, callback) {
    auth.exec(gen);
    function gen(auth) {
        const drive = google.drive({version: 'v3', auth});
        drive.files[funcName](props, function (err, result) {
            if (err) {
                // Handle error
                console.error(err);
                if (typeof callback === "function")  {
                    callback(err, null);
                }
            } else {
                if (typeof callback === "function") {
                    callback(null, result);
                }
            }
        });
    }
}

function uploadFile(file, callback) {    
    
    var fileMetadata = {
        'name': file
    };

    var media = {
        mimeType: 'text/plain',
        body: fs.createReadStream(file)
    };

    generic("create", {
        resource: fileMetadata,
        media: media,
        fields: 'id'
    }, callback);
}

function listFiles(callback) {
    generic("list", {}, callback);
}

function getFile(fileId, callback) {
    generic("get", { "fileId": fileId, "fields": "id, name, size, createdTime" }, callback);
}

function deleteFile(fileId, callback) {
    generic("delete", { "fileId": fileId }, callback);
}

module.exports = {
    uploadFile: uploadFile,
    listFiles: listFiles,
    getFile: getFile,
    deleteFile: deleteFile
};