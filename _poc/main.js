var client = require("./client");
a();
function a() {
    client.uploadFile("files/test.txt", function(err, file) {
        if (!(typeof err === "undefined" || err === null)) {
            console.error("There was an error in uploading the file");
        }
    
        console.log("Success");
        console.log("Uploaded file id: " + file.data.id);

        b();
    });
}

function b() {
    var fileIds = [];
    client.listFiles(function(err, fileList) {
        if (fileList && fileList.data.files.length > 0) {
            fileList.data.files.forEach(function(file) {
                console.log("File: " + file.id);
            });
            c(fileList.data.files[0]);
        }
    });
}

function c(file) {
    client.getFile(file.id, (err, f) => {
        if (err) {
            console.error("Error happened");
        }
        else {
            console.log("id: " + f.data.id);
            console.log("name: " + f.data.name);
            console.log("size: " + f.data.size);
            console.log("created: " + f.data.createdTime);
            d({
                "id":f.data.id,
                "name":f.data.name,
                "size":f.data.size,
                "createdTime":f.data.createdTime
            })
        }
    });
}

function d(file) {
    client.deleteFile(file.id, (err) => {
        if (err) 
        {
            console.error("Error deleting file: " + file.id);
        }
        else 
        {
            console.log("Successfully deleted file: " + file.name);
        }
    });
}