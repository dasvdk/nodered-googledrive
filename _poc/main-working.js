var auth = require("./auth")
const fs = require('fs');
const {google} = require('googleapis');

auth.exec(uploadFile);

function listFiles(auth) {
    const drive = google.drive({version: 'v3', auth});
    drive.files.list({
        pageSize: 10,
        fields: 'nextPageToken, files(id, name)',
    }, (err, res) => {
        if (err) return console.log('The API returned an error: ' + err);
        const files = res.data.files;
        if (files.length) {
        console.log('Files:');
        files.map((file) => {
            console.log(`${file.name} (${file.id})`);
        });
        } else {
        console.log('No files found.');
        }
    });
}


function uploadFile(auth) {
    const drive = google.drive({version: 'v3', auth});

    var fileMetadata = {
        'name': 'test.txt'
    };

    var media = {
        mimeType: 'text/plain',
        body: fs.createReadStream('files/test.txt')
    };

    drive.files.create({
        resource: fileMetadata,
        media: media,
        fields: 'id'
    }, function (err, file) {
        if (err) {
            // Handle error
            console.error(err);
        } else {
            console.log('File Id: ', file.id);
        }
    });
}
